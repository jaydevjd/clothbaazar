﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClothBaazar.Web.Startup))]
namespace ClothBaazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
